CREATE TABLE `officedb`.`staff` (
  `idStaff` VARCHAR(5) NOT NULL,
  `namaStaff` VARCHAR(30) NOT NULL,
  `umurStaff` INT(2) UNSIGNED NOT NULL,
  `idManager` VARCHAR(5) NOT NULL,
  PRIMARY KEY (`idStaff`));
  
CREATE TABLE `officedb`.`manager` (
  `idManager` VARCHAR(5) NOT NULL,
  `namaManager` VARCHAR(30) NOT NULL,
  `idDepartemen` VARCHAR(5) NOT NULL,
  PRIMARY KEY (`idStaff`));
  
INSERT INTO `officedb`.`staff`
(`idStaff`,
`namaStaff`,
`umurStaff`,
`idManager`)
VALUES
('E101',
'Cimi',
'18',
'M404'),
('E102',
'Andri',
'20',
'M401'),
('E103',
'Ahmad',
'17',
'M123'),
('E104',
'Faris',
'27',
'M411'),
('E105',
'Zia',
'20',
'M404'),
('E106',
'Kantin',
'28',
'M403');

INSERT INTO `officedb`.`manager`
(`idManager`,
`namaManager`,
`idDepartemen`)
VALUES
('M123',
'Kuda',
'D1011'),
('M401',
'Kucing',
'D1011'),
('M403',
'Kancil',
'D1022'),
('M404',
'Kuda',
'D1088'),
('M411',
'Kuda',
'D2099');



